gitlab2pandas package
=====================

gitlab2pandas.core module
-------------------------

.. automodule:: gitlab2pandas.core
   :members:
   :undoc-members:
   :show-inheritance:

gitlab2pandas.extractions module
--------------------------------

.. automodule:: gitlab2pandas.extractions
   :members:
   :undoc-members:
   :show-inheritance:

gitlab2pandas.gitlab2pandas module
----------------------------------

.. automodule:: gitlab2pandas.gitlab2pandas
   :members:
   :undoc-members:
   :show-inheritance:

gitlab2pandas.processing module
-------------------------------

.. automodule:: gitlab2pandas.processing
   :members:
   :undoc-members:
   :show-inheritance:

Change log
==========

Version 0.0.1 (Sep 13, 2022)
-----------------------------------

* First version of gitlab2pandas
* Extract GitLab information
* Save to pandas files or save to a xlsx files
* Process the data (replace column names & pseudonymise user_ids)

Version 0.0.2 (Sep 19, 2022)
-----------------------------------

* Fix bugs in extraction
* Add a update functionality to the extraction
* Add documentation in the code
* Publish code on github.com
* rework tests for gitlab2pandas

Version 0.0.3 (Sep 26, 2022)
-----------------------------------

* Rename project_data_dit --> project_data_dir
* Add get path of pandas dataframe by feature name
* Specify parent_id for commits refs and defs
* Update date frame only if file exists
* Remove name_parent_ids in processing because all parent_ids should already be renamed

Version 0.0.4 (Sep 27, 2022)
-----------------------------------

* Can use JSON file as input and export as output


Version 0.0.5 (Feb 16, 2023)
-----------------------------------

* Return a Path object for def get_pandas_data_frame_path()
* removes redundant information in wiki, when a project is downloaded mutiple times
* add a citation file for the project
import os

DATA_ROOT_DIR = "test_data"

SERVER_URL = "https://gitlab.com/"
PROJECT_NAMESPACE = "gitlab-learning-analytics"
PROJECT_NAME = "gitlab2pandas"

LOCAL_TOKEN_KEY = "PRIVATE_TOKEN"
LOCAL_SERVER_URL_KEY = "LOCAL_SERVER_URL"
LOCAL_PROJECT_NAMESPACE_KEY = "LOCAL_PROJECT_NAMESPACE"
LOCAL_PROJECT_NAME_KEY = "LOCAL_PROJECT_NAME"


def server_url() -> str:
    if "CI_JOB_TOKEN" in os.environ:
        return SERVER_URL
    else:
        return os.environ[LOCAL_SERVER_URL_KEY]

def project_namespace() -> str:
    if "CI_JOB_TOKEN" in os.environ:
        return PROJECT_NAMESPACE
    else:
        return os.environ[LOCAL_PROJECT_NAMESPACE_KEY]

def project_name() -> str:
    if "CI_JOB_TOKEN" in os.environ:
        return PROJECT_NAME
    else:
        return os.environ[LOCAL_PROJECT_NAME_KEY]

def connect(gitlab2pandas_object):
    if "CI_JOB_TOKEN" in os.environ:
        gitlab2pandas_object.connect(SERVER_URL,job_token=os.environ["CI_JOB_TOKEN"])
    else:
        gitlab2pandas_object.connect(server_url(),private_token=os.environ[LOCAL_TOKEN_KEY])